# Categories

This is an examplar project for how to not write good Elixir. :)

I'm new to Elixir and Functional programming and am stuck on something I need to get through.  I need to refactor some code to allow for maintainability, and extensibility.

Here's the basics.  I need to look into a directory structure and determine the "category"
for it, based on the existence of a specific file name.  The categories, and their files are
stored in a struct as a Map of key:values, with the the "key" being the categories, and the
values being a List of filenames that designate that particular category.

* The directory structure is nested.
* A directory can belong to multiple categories.

The expected output is defined in the tests, and examples are below. 

This project contains the basic struct, test cases, and a really bad implementation
of determining categorization.

The test data is in the `test/dirs` subdirectory structure and it looks like this:

```
├── dir1
│   └── type1
├── dir2
│   ├── subdir1
│   │   └── type1
│   ├── subdir2
│   │   └── type2
│   ├── type1
│   └── type2.1
└── dir3
    ├── subdir1
    │   └── type3
    ├── subdir2
    │   └── type1
    ├── subdir3
    │   └── type2
    ├── type1
    ├── type1.1
    ├── type2
    └── type3
```

Categories are: `cat1` and its types are `type1` and `type1.1`; `cat2` and its types are `type2`
and `type2.1`; `cat3` and its type is `type3`.  Each directory in `dirs` is a unique test project.

So `dir1` has a `type1` file making it a `cat1` project.  And `dir2` has `type1` files as well as
`type2` and `type2.1` making it a `cat1` and `cat2` project.  And so on...with `dir3`.

The result of the categorization function for `dir1` is:

```
%{
  "type1" => ["test/dirs/dir1/type1"]
}
```

For `dir2` it is:

```
%{
  "type1" => ["test/dirs/dir2/type1", "test/dirs/dir2/subdir1/type1],
  "type2" => ["test/dirs/dir2/type2.1", "test/dirs/dir2/subdir2/type2]
}
```

The current functionality works, and passes the tests.  But, adding a new category will require new code.  Help!?!


   
