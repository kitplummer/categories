defmodule CategoriesTest do
  use ExUnit.Case, async: true
  doctest Categories

  setup_all do
    category_one = %Category{name: :cat1, types: ["type1", "type1.1"]}
    category_two = %Category{name: :cat2, types: ["type2", "type2.1"]}
    category_three = %Category{name: :cat3, types: ["type3"]}

    ## TODO: Make this an extensible list of categories, no keys
    cat_map = %{:cat1 => category_one, :cat2 => category_two, :cat3 => category_three}
    [categories: cat_map]
  end

  describe "greets the world with bad Elixir" do
    test "dir1", %{categories: categories} do
      expected = %{
        cat1: ["test/dirs/dir1/type1"]
      }

      assert Categories.categorize!("test/dirs/dir1", categories) == expected
    end

    test "dir2", %{categories: categories} do
      expected = %{
        cat1: ["test/dirs/dir2/subdir1/type1", "test/dirs/dir2/type1"],
        cat2: ["test/dirs/dir2/subdir2/type2", "test/dirs/dir2/type2.1"]
      }

      assert Categories.categorize!("test/dirs/dir2", categories) == expected
    end

    test "dir3", %{categories: categories} do
      expected = %{
        cat1: ["test/dirs/dir3/subdir2/type1", "test/dirs/dir3/type1", "test/dirs/dir3/type1.1"],
        cat2: ["test/dirs/dir3/subdir3/type2", "test/dirs/dir3/type2"],
        cat3: ["test/dirs/dir3/subdir1/type3", "test/dirs/dir3/type3"]
      }

      assert Categories.categorize!("test/dirs/dir3", categories) == expected
    end
  end
end
