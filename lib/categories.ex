defmodule Categories do
  def categorize!(directory, categories) do
    result = %{}
    cat1 = find_types_for_cat(directory, categories[:cat1])

    result =
      if Kernel.length(cat1[:cat1]) > 0 do
        Map.merge(result, cat1)
      else
        result
      end

    cat2 = find_types_for_cat(directory, categories[:cat2])

    result =
      if Kernel.length(cat2[:cat2]) > 0 do
        Map.merge(result, cat2)
      else
        result
      end

    cat3 = find_types_for_cat(directory, categories[:cat3])

    result =
      if Kernel.length(cat3[:cat3]) > 0 do
        Map.merge(result, cat3)
      else
        result
      end

    result
  end

  def find_types_for_cat(directory, category) do
    %{category.name => Path.wildcard("#{directory}/**/#{Category.types_to_string(category)}")}
  end
end
