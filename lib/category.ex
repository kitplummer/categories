defmodule Category do
  alias __MODULE__

  defstruct name: nil, types: []

  def types_to_string(category) do
    types = Enum.join(category.types, ",")
    "{" <> types <> "}"
  end
end
